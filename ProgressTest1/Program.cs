﻿using System;
//Bai tap Progress Test 01 - PRN212 - Pham Gia Huy - HE170722
class Program
{
    static void Main(String[] args)
    {

        int numberOfArray = 0;
        double[] array = new double[50];
        int choice;
        do
        {
            Menu();
            //ask the user to enter a choice, if the user enters a number that is not in the menu or not is a number, the program will ask the user to re-enter          

            while (!int.TryParse(Console.ReadLine(), out choice))
            {
                Console.WriteLine("!!! Ban can nhap so chinh xac theo cac lua chon o ben tren");
                Console.Write("=> Nhap lua chon cua ban: ");
            }
            switch(choice)
            {
                case 1:
                    Console.Write("Nhap N (0<N<50): ");
                    numberOfArray = InputNumberOfArray();
                    break;
        
                case 2:
                    Console.WriteLine("Nhap mang gom N so thuc");
                    for(int i = 0; i < numberOfArray; i++)
                    {
                        Console.Write("Nhap phan tu thu " + (i + 1) + ": ");
                        array[i] = InputArray();
                    }
                    break;
                case 3:
                    Console.WriteLine("So lon nhat trong mang la: " + FindMax(array, numberOfArray));
                    break;
                case 4:
                    Console.WriteLine("So nho nhat trong mang la: " + FindMin(array, numberOfArray));
                    break;
                case 5:
                    FindMaxEvenPositive(array, numberOfArray);
                    break;
                case 6:
                    FindMinOddNegative(array, numberOfArray);
                    break;
                case 7:
                    FindSquare(array, numberOfArray);
                    break;
                case 8:
                    Console.WriteLine("Tong mang la: " + SumArray(array, numberOfArray));
                    break;
                case 9:
                    Console.WriteLine("Trung binh cong cac phan tu mang la: " + AverageArray(array, numberOfArray));
                    break;
                case 10:
                    FindElementGreaterThanAverage(array, numberOfArray);
                    break;
                case 11:
                    SortArrayAscending(array, numberOfArray);
                    Console.WriteLine("Mang sau khi sap xep tang dan la: ");
                    for(int i = 0; i < numberOfArray; i++) Console.Write(array[i] + " ");
                    Console.WriteLine();
                    break;
                case 12:
                    SortArrayDescending(array, numberOfArray);
                    Console.WriteLine("Mang sau khi sap xep giam dan la: ");
                    for (int i = 0; i < numberOfArray; i++) Console.Write(array[i] + " ");
                    Console.WriteLine();
                    break;
                case 13:
                    PrintAverageOfNonNegative(array, numberOfArray);
                    break;
                case 0:
                    Console.WriteLine("Thoat");
                    break;
                default:
                    Console.WriteLine("!!! Ban can nhap so chinh xac theo cac lua chon o ben tren");
                    break;
            }
        } while(choice != 0);
      
    }
    static void Menu()
    {
        Console.WriteLine("=========================MENU===============================");
        Console.WriteLine("==    1.  Nhap so nguyen N (0<N<50)");
        Console.WriteLine("==    2.  Nhap mot mang gom N so thuc");
        Console.WriteLine("==    3.  Tim so lon nhat trong mang");
        Console.WriteLine("==    4.  Tim so nho nhat trong mang");
        Console.WriteLine("==    5.  Tim so duong chan lon nhat trong mang");
        Console.WriteLine("==    6.  Tim so am le nho nhat trong mang");
        Console.WriteLine("==    7.  Tim cac so chinh phuong trong mang");
        Console.WriteLine("==    8.  Tinh tong mang");
        Console.WriteLine("==    9.  Tinh trung binh cong cac phan tu mang");
        Console.WriteLine("==    10. Tim nhung phan tu lon hon trung binh cong");
        Console.WriteLine("==    11. Sap xep mang theo thu tu tang dan");
        Console.WriteLine("==    12. Sap xep mang theo thu tu giam dan");
        Console.WriteLine("==    13. In ra trung binh cong cac phan tu khong am cua day");
        Console.WriteLine("==    0. Thoat");
        Console.WriteLine("===========================================================");
        Console.Write("=> Nhap lua chon cua ban: ");
    }


    //Allowing the user to enter the number of elements in array, if the input is not a number or not in the range, the program will ask the user to re-enter
    static int InputNumberOfArray()
    {
        int numberOfArray;
        while (!int.TryParse(Console.ReadLine(), out numberOfArray) || numberOfArray <= 0 || numberOfArray >= 50)
        {
            Console.WriteLine("!!! Ban can nhap N (0<N<50)");
            Console.Write("=> Nhap N: ");
        }
        return numberOfArray;
    }

    //Allowing the user to enter the elements of the array, if the input is not a number, the program will ask the user to re-enter
    static double InputArray()
    {
        double number;
        while (!double.TryParse(Console.ReadLine(), out number))
        {
            Console.WriteLine("!!! Ban can nhap so thuc");
            Console.Write("=> Nhap lai: ");
        }
        return number;
    }

    //Find the max number
    static double FindMax(double[] array, int numberOfArray)
    {
        double max = array[0];
        for(int i = 1; i < numberOfArray; i++) { if (array[i] > max) max = array[i]; }
        return max;
    }

    //Find the min number
    static double FindMin(double[] array, int numberOfArray)
    {
        double min = array[0];
        for(int i = 1; i < numberOfArray; i++) { if (array[i] < min) min = array[i]; }
        return min;
    }

    //Find the largest even positive number in the array, if there is no even positive number, print error message
    static void FindMaxEvenPositive(double[] array, int numberOfArray)
    {
        double max = array[0];
        for(int i = 1; i < numberOfArray; i++) { if (array[i] > max && array[i] % 2 == 0 && array[i] > 0) max = array[i]; }
        if(max % 2 != 0 || max < 0) Console.WriteLine("Khong co so duong chan trong mang");
        else Console.WriteLine("So duong chan lon nhat trong mang la: " + max);
    }

    static void FindMinOddNegative(double[] array, int numberOfArray)
    {
        double min = array[0];
        for (int i = 1; i < numberOfArray; i++) { if (array[i] < min && array[i] % 2 != 0 && array[i] < 0) min = array[i]; }
        if (min % 2 == 0 || min > 0) Console.WriteLine("Khong co so am le trong mang");
        else Console.WriteLine("So am le nho nhat trong mang la: " + min);
    }

    //Find the square numbers in the array, if there is no square number, print cannot find
    static void FindSquare(double[] array, int numberOfArray)
    {
        bool check = false;
        for(int i = 0; i < numberOfArray; i++)
        {
            if (Math.Sqrt(array[i]) % 1 == 0)
            {
                Console.WriteLine("So chinh phuong trong mang la: " + array[i]);
                check = true;
            }
        }
        if(check == false) Console.WriteLine("Khong co so chinh phuong trong mang");
    }
   
    //Calculate the sum of the array
    static double SumArray(double[] array, int numberOfArray)
    {
        double sum = 0;
        for(int i = 0; i < numberOfArray; i++) sum += array[i];
        return sum;
    }

    //Calculate the average of the array
    static double AverageArray(double[] array, int numberOfArray)
    {
        return SumArray(array, numberOfArray) / numberOfArray;
    }

    //Find the elements greater than the average of array
    static void FindElementGreaterThanAverage(double[] array, int numberOfArray)
    {
        double average = AverageArray(array, numberOfArray);
        for(int i = 0; i < numberOfArray; i++)
        {
            if (array[i] > average) Console.WriteLine("Phan tu lon hon trung binh cong la: " + array[i]);
        }
    }

    //Sort the array in ascending order by using bubble sort
    static void SortArrayAscending(double[] array, int numberOfArray)
    {
        for(int i = 0; i < numberOfArray - 1; i++)
        {
            for(int j = i + 1; j < numberOfArray; j++)
            {
                if (array[i] > array[j])
                {
                    double temp = array[i];
                    array[i] = array[j];
                    array[j] = temp;
                }
            }
        }
    }

    //Sort the array in descending order by using bubble sort
    static void SortArrayDescending(double[] array, int numberOfArray)
    {
        for (int i = 0; i < numberOfArray - 1; i++)
        {
            for (int j = i + 1; j < numberOfArray; j++)
            {
                if (array[i] < array[j])
                {
                    double temp = array[i];
                    array[i] = array[j];
                    array[j] = temp;
                }
            }
        }
    }

    //Print the average of the elements that are not negative
    static void PrintAverageOfNonNegative(double[] array, int numberOfArray)
    {
        double sum = 0;
        int count = 0;
        for(int i = 0; i < numberOfArray; i++)
        {
            if (array[i] >= 0)
            {
                sum += array[i];
                count++;
            }
        }
        if(count == 0) Console.WriteLine("Khong co phan tu khong am cu day");
        else Console.WriteLine("Trung binh cong cac phan tu khong am cu day la: " + sum / count);
    }

}